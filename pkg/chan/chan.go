package gtchan

import (
	"reflect"
	"unsafe"
)

// IsChan
//
//	@Description: Проверка, является ли переданный аргумент каналом.
//	@param ch Аргумент.
//	@return bool true-канал, false-не канал
func IsChan(ch interface{}) bool {
	return reflect.TypeOf(ch).Kind() == reflect.Chan
}

// IsClosed
//
//	@Description: Проверить, закрыт канал или нет.
//	@param ch Канал для проверки.
//	@return bool true-закрыт, false-открыт.
func IsClosed(ch interface{}) bool {
	if !IsChan(ch) {
		panic("is not a channel")
	}

	cptr := *(*uintptr)(unsafe.Pointer(uintptr(unsafe.Pointer(&ch)) + unsafe.Sizeof(uint(0))))

	cptr += unsafe.Sizeof(uint(0)) * 2
	cptr += unsafe.Sizeof(unsafe.Pointer(uintptr(0)))
	cptr += unsafe.Sizeof(uint16(0))
	return *(*uint32)(unsafe.Pointer(cptr)) > 0
}
