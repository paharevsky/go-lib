package gtchan

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestChan_IsChan(t *testing.T) {
	var (
		isChan  chan int
		notChan int
	)
	require.True(t, IsChan(isChan), "Define chan variable as a non chan")
	require.False(t, IsChan(notChan), "Define not chan variable as a chan")
}

func TestChan_IsClosed(t *testing.T) {
	var (
		openedChan = make(chan int)
		closedChan = make(chan int)
	)
	defer close(openedChan)
	close(closedChan)

	require.True(t, IsClosed(closedChan), "Define closed chan as opened")
	require.False(t, IsClosed(openedChan), "Define opened chan as closed")

	defer func() {
		if r := recover(); r == nil {
			t.Error("expected panic")
		}
	}()

	IsClosed(1)
}
