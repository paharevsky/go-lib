// Package gtcfg
// @Description: Инструменты для работы с конфигами.
package gtcfg

import (
	"errors"
	"github.com/ory/viper"
	"os"
	"path/filepath"
	"strings"
)

var ErrPathIsEmpty = errors.New("path to file is empty")

var ErrInvalidFileName = errors.New("invalid file name")

// New New[T any]
//
//	@Description: Считать конфигурацию из файла.
//	@param pathToFile Путь до файла конфигурации (с названием и расширением файла).
//	@return *T Считанная конфигурация.
//	@return error Ошибка.
func New[T any](pathToFile string) (*T, error) {
	if strings.TrimSpace(pathToFile) == "" {
		return nil, ErrPathIsEmpty
	}

	path := filepath.Dir(pathToFile)
	file := filepath.Base(pathToFile)

	split := strings.Split(file, ".")
	if len(split) < 2 {
		return nil, ErrInvalidFileName
	}

	fileName := strings.Join(split[:len(split)-1], ".")
	fileExt := split[len(split)-1]

	cfg := new(T)

	viper.AutomaticEnv()

	viper.AddConfigPath(path)
	viper.SetConfigType(fileExt)
	viper.SetConfigName(fileName)

	err := viper.ReadInConfig()
	if err != nil {
		return nil, err
	}

	err = viper.Unmarshal(&cfg)
	if err != nil {
		return nil, err
	}

	return cfg, err
}

// TryNewWithArgs TryNewWithArgs[T any]
//
//	@Description: Считать конфигурацию из файла, по возможности считав путь до файла из cmd args.
//	@param argsPos Позиция в args, где предполагаемо находится путь до файла конфигурации.
//	@param catchPathToFile Путь до файла конфигурации, в случае если len(args) < argsPos+1
//	@return *T Считанная конфигурация.
//	@return error Ошибка.
func TryNewWithArgs[T any](argsPos int, catchPathToFile string) (*T, error) {
	if len(os.Args) >= argsPos+1 {
		catchPathToFile = os.Args[argsPos]
	}
	return New[T](catchPathToFile)
}
