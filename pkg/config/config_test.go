package gtcfg

import (
	"github.com/stretchr/testify/require"
	"testing"
)

var TestNew = []struct {
	name        string
	path        string
	DataType    interface{}
	expectedErr bool
}{
	{"Ok", "test_conf/test_conf.yml", map[string]interface{}{}, false},
	{"Empty Path", "", map[string]interface{}{}, true},
	{"Invalid Path", "/", map[string]interface{}{}, true},
	{"Invalid Config", "test_conf/invalid_conf.txt", map[string]interface{}{}, true},
	{"Invalid type", "test_conf/test_conf.yml", map[int]int{}, true},
}

func TestConfig_New(t *testing.T) {
	var (
		cfg any
		err error
	)
	for _, test := range TestNew {
		switch test.DataType.(type) {
		case map[string]interface{}:
			cfg, err = New[map[string]interface{}](test.path)
		case map[int]int:
			cfg, err = New[map[int]int](test.path)
		}

		if test.expectedErr {
			require.Error(t, err, test.name)
		} else {
			require.Nilf(t, err, "In test %s unexpected error: %s", test.name, err)
			require.NotNilf(t, cfg, "In test %s unexpected nil config", test.name)
		}
	}
}

func TestConfig_TryNewWithArgs(t *testing.T) {
	_, err := TryNewWithArgs[map[string]interface{}](2, "test_conf")
	require.Error(t, err, "Test TryNewWithArgs")

}
