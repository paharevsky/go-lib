package gtstring

import "unicode"

// FirstLetterToLower перевести первый символ строки в нижний регистр.
func FirstLetterToLower(s string) string {
	if len(s) == 0 {
		return s
	}

	r := []rune(s)
	r[0] = unicode.ToLower(r[0])

	return string(r)
}
