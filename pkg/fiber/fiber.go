package gtfiber

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	log "github.com/sirupsen/logrus"
	gthttp "gitlab.com/paharevsky/go-lib/pkg/http"
	gtvalidate "gitlab.com/paharevsky/go-lib/pkg/validate"
)

type (
	Route struct {
		Method                string
		Path                  string
		Handler               fiber.Handler
		Middleware            []fiber.Handler
		RewriteGlobalPrefixTo string
		DisableGlobalPrefix   bool
	}
)

type (
	Module interface {
		Routes() []Route
	}
)

// DefineFiberApiRoutesConfig
// @Description: Конфигурация для функции регистрации модулей.
//
//	@field App Объект fiber app.
//	@field Modules Функция, в которой объявляются модули приложения.
//	@field ServiceName Название приложения/сервиса, который обслуживает эти роуты.
//	@field GlobalPrefix Перфикс для всех роутов API.
//	@field PrintRoutes Вывести или нет в консоль зарегистрированные роуты.
type DefineFiberApiRoutesConfig struct {
	App          *fiber.App                    `validate:"required"`
	FnModules    func(app *fiber.App) []Module `validate:"required"`
	ServiceName  string                        `validate:"required"`
	GlobalPrefix string
	PrintRoutes  bool
	Logger       *log.Logger
}

// DefineFiberApiRoutes
//
//	@Description: Зарегистрировать модули.
//	@param e Объект fiber app.
//	@param modules Функция, в которой объявляются модули приложения.
//	@param serviceName Название приложения/сервиса, который обслуживает эти роуты.
//	@param printRoutes Вывести или нет в консоль зарегистрированные роуты.
//	@return error
func DefineFiberApiRoutes(cfg DefineFiberApiRoutesConfig) error {
	v := gtvalidate.Default()
	if err := v.Struct(cfg); err != nil {
		return err
	}

	modules := cfg.FnModules(cfg.App)
	routes := make([]Route, 0)

	for _, module := range modules {
		if module != nil {
			routes = append(routes, module.Routes()...)
		}
	}

	allRoutes := fmt.Sprint("\n[" + cfg.ServiceName + "]\n")
	for _, route := range routes {
		prefix := cfg.GlobalPrefix

		if route.RewriteGlobalPrefixTo != "" {
			prefix = route.RewriteGlobalPrefixTo
		}

		if route.DisableGlobalPrefix {
			prefix = ""
		}

		api := cfg.App.Group(prefix)

		fullRoutePath := fmt.Sprintf("%s%s", prefix, route.Path)

		prevAllRoutes := ""
		if cfg.PrintRoutes {
			prevAllRoutes = allRoutes
			allRoutes += fmt.Sprint(route.Method, "\t", fullRoutePath, "\n")
		}

		handlers := make([]fiber.Handler, 0, len(route.Middleware)+1)
		handlers = append(handlers, route.Middleware...)
		handlers = append(handlers, route.Handler)

		switch route.Method {
		case gthttp.POST:
			{
				api.Post(route.Path, handlers...)
				break
			}
		case gthttp.GET:
			{
				api.Get(route.Path, handlers...)
				break
			}
		case gthttp.DELETE:
			{
				api.Delete(route.Path, handlers...)
				break
			}
		case gthttp.PUT:
			{
				api.Put(route.Path, handlers...)
				break
			}
		case gthttp.PATCH:
			{
				api.Patch(route.Path, handlers...)
				break
			}
		case gthttp.HEAD:
			{
				api.Head(route.Path, handlers...)
			}
		default:
			{
				allRoutes = prevAllRoutes
				allRoutes += fmt.Sprint("unknown method '", route.Method, "'\t", fullRoutePath, "\n")
			}
		}
	}

	if cfg.PrintRoutes {
		if cfg.Logger != nil {
			cfg.Logger.Println(allRoutes)
		} else {
			fmt.Println(allRoutes)
		}
	}

	return nil
}
