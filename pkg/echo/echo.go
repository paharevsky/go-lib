// Package gtecho
// @Description: Инструменты для модулей и хендлеров, использующих echo framework.
package gtecho

import (
	"errors"
	"fmt"
	"reflect"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	log "github.com/sirupsen/logrus"
	gtserver "gitlab.com/paharevsky/go-lib/pkg/server"
	gtslice "gitlab.com/paharevsky/go-lib/pkg/slice"
	gtstring "gitlab.com/paharevsky/go-lib/pkg/string"
	gtvalidate "gitlab.com/paharevsky/go-lib/pkg/validate"
)

var (
	ErrRequireValidator = errors.New("require validator")
)

type (
	Route struct {
		Method                string
		Path                  string
		Handler               echo.HandlerFunc
		Middleware            []echo.MiddlewareFunc
		RewriteGlobalPrefixTo string
		DisableGlobalPrefix   bool
		Group                 string
	}

	EchoValidator struct {
		Core *validator.Validate
	}

	ValidationError struct {
		//Namespace string `json:"namespace"`
		Field   string `json:"field"`
		Tag     string `json:"tag"`
		Message string `json:"message"`
	}

	ValidationErrors []ValidationError

	UserRole string

	// J Сокращение, для удобного JSON ответа клиенту.
	J map[string]any

	EchoConfig struct {
		AllowOrigins          []string
		AllowMethods          []string
		Validator             *EchoValidator
		GlobalMiddlewares     []echo.MiddlewareFunc
		DisableDefaultLogger  bool
		DisableDefaultRecover bool
	}

	Module interface {
		Routes() []Route
	}

	Echo struct {
		Instance  *echo.Echo
		Validator *EchoValidator
	}
)

// New
//
//	@Description: Создать экземпляр объекта фреймворка Echo.
//	@param cfg Конфигурация конструктора.
//	@return *echo.Echo Объект фреймворка Echo.
//	@return error
func New(echoConfig ...EchoConfig) (*Echo, error) {
	e := echo.New()
	var (
		_validator *EchoValidator
		cfg        EchoConfig
	)

	if len(echoConfig) > 0 {
		cfg = echoConfig[0]
	}

	if cfg.Validator == nil {
		_validator = &EchoValidator{Core: validator.New()}
	} else if cfg.Validator != nil {
		_validator = cfg.Validator
	}
	e.Validator = _validator

	e.Use(cfg.GlobalMiddlewares...)

	if !cfg.DisableDefaultLogger {
		e.Use(middleware.Logger())
	}

	if !cfg.DisableDefaultRecover {
		e.Use(middleware.Recover())
	}

	if cfg.AllowOrigins == nil || len(cfg.AllowOrigins) == 0 {
		cfg.AllowOrigins = []string{"*"}
	}

	if cfg.AllowMethods == nil || len(cfg.AllowMethods) == 0 {
		cfg.AllowMethods = []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE}
	}

	// CORS middleware для эндпоинтов API.
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: cfg.AllowOrigins,
		AllowMethods: cfg.AllowMethods,
	}))

	return &Echo{
		Instance:  e,
		Validator: _validator,
	}, nil
}

// RegisterModulesConfig
// @Description: Конфигурация для функции регистрации модулей.
//
//	@field Echo Объект echo.
//	@field Modules Функция, в которой объявляются модули приложения.
//	@field AppName Название приложения/сервиса, который обслуживает эти роуты.
//	@field Logger Логгер.
//	@field GlobalPrefix Перфикс для всех роутов API.
//	@field PrintRoutes Вывести или нет в консоль зарегистрированные роуты.
type RegisterModulesConfig struct {
	Echo             *echo.Echo
	Modules          func(e *echo.Echo) []Module
	AppName          string
	Logger           *log.Logger
	GlobalPrefix     string
	PrintRoutes      bool
	RoutesLogOneLine bool
	AllowGroups      []string
}

// DeclareModules
//
//	@Description: Зарегистрировать модули.
//	@param e Объект echo.
//	@param modules Функция, в которой объявляются модули приложения.
//	@param serviceName Название приложения/сервиса, который обслуживает эти роуты.
//	@param printRoutes Вывести или нет в консоль зарегистрированные роуты.
//	@return error
func DeclareModules(cfg RegisterModulesConfig) error {
	if cfg.Echo == nil {
		return fmt.Errorf("echo is required")
	}

	if cfg.AppName == "" {
		return fmt.Errorf("serviceName is required")
	}

	if cfg.Modules == nil {
		return fmt.Errorf("modules is required")
	}

	controllers := cfg.Modules(cfg.Echo)
	routes := make([]Route, 0)

	for _, controller := range controllers {
		if controller != nil {
			routes = append(routes, controller.Routes()...)
		}
	}

	allRoutes := ""
	if cfg.RoutesLogOneLine {
		allRoutes = fmt.Sprintf("[%v]", cfg.AppName)
	} else {
		allRoutes = fmt.Sprintf("\n[%v]\n", cfg.AppName)
	}

	for _, route := range routes {
		if cfg.AllowGroups != nil && !gtslice.Contains(cfg.AllowGroups, route.Group) {
			continue
		}

		prefix := cfg.GlobalPrefix

		if route.RewriteGlobalPrefixTo != "" {
			prefix = route.RewriteGlobalPrefixTo
		}

		if route.DisableGlobalPrefix {
			prefix = ""
		}

		api := cfg.Echo.Group(prefix)

		fullRoutePath := fmt.Sprintf("%s%s", prefix, route.Path)

		if cfg.PrintRoutes {
			if cfg.RoutesLogOneLine {
				allRoutes += fmt.Sprint(" (", route.Method, " ", fullRoutePath, ")")
			} else {
				allRoutes += fmt.Sprint(route.Method, "\t", fullRoutePath, "\n")
			}
		}

		switch route.Method {
		case echo.POST:
			{
				api.POST(route.Path, route.Handler, route.Middleware...)
				break
			}
		case echo.GET:
			{
				api.GET(route.Path, route.Handler, route.Middleware...)
				break
			}
		case echo.DELETE:
			{
				api.DELETE(route.Path, route.Handler, route.Middleware...)
				break
			}
		case echo.PUT:
			{
				api.PUT(route.Path, route.Handler, route.Middleware...)
				break
			}
		case echo.PATCH:
			{
				api.PATCH(route.Path, route.Handler, route.Middleware...)
				break
			}
		case echo.HEAD:
			{
				api.HEAD(route.Path, route.Handler, route.Middleware...)
			}
		}
	}

	if cfg.PrintRoutes {
		if len(controllers) == 0 {
			allRoutes += " no routes"
		}

		if cfg.Logger != nil {
			cfg.Logger.Println(allRoutes)
		} else {
			fmt.Println(allRoutes)
		}
	}

	return nil
}

// Error
//
//	@Description: Имплементация метода Error для глобального интерфейса error.
//	@receiver ve
//	@return string
func (ve ValidationErrors) Error() string {
	sErrs := make([]string, len(ve))
	for i, validationError := range ve {
		sErrs[i] = validationError.Message
	}
	return strings.Join(sErrs, "\n")
}

// Validate
//
//	@Description: Проверка request body.
//	@receiver cv
//	@param i Структура для проверки всех обязательных полей.
//	@return error
func (ev *EchoValidator) Validate(i interface{}) error {
	if reflect.TypeOf(i).Kind() == reflect.Slice {
		err := ev.Core.Var(i, "min=1,dive")
		return err
	}

	if err := ev.Core.Struct(i); err != nil {
		validationErrors := err.(validator.ValidationErrors)
		var errors ValidationErrors
		for _, e := range validationErrors {
			field := gtstring.FirstLetterToLower(e.Field())
			tag := e.Tag()

			errors = append(errors, ValidationError{
				//Namespace: e.Namespace(),
				Field:   field,
				Tag:     tag,
				Message: fmt.Sprintf("field validation for '%s' failed on the '%s' tag", field, tag),
			})
		}
		return errors
	}
	return nil
}

// EchoPrintRoutes
//
//	@Description: Вывести в консоль список зарегистрированных роутов, используя Echo framework.
//	@param e Объект echo.
func EchoPrintRoutes(e *echo.Echo) {
	for _, route := range e.Routes() {
		if route != nil {
			EchoPrintRoute(route)
		}
	}
}

// EchoPrintRoute
//
//	@Description: Вывести в консоль роут, используя Echo framework.
//	@param route Echo роут.
func EchoPrintRoute(route *echo.Route) {
	fmt.Println("["+route.Method+"]", route.Path)
}

// EchoLoggerMiddleware
//
//	@Description: Промежуточный обработчик, выводящий информацию о запросе.
//	@param e Объект echo.
func EchoLoggerMiddleware(e *echo.Echo) {
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "${time_rfc3339} [${method} ${status}] ${uri}\n",
	}))
}

type NewAppCfg struct {
	AppName              string                      `validate:"required"`
	Port                 uint                        `validate:"required"`
	Modules              func(e *echo.Echo) []Module `validate:"required"`
	ServerLogger         *log.Logger
	DeclareModulesLogger *log.Logger
	PrintRoutes          bool
	EchoConfig           EchoConfig
	GlobalPrefix         string
	RoutesLogOneLine     bool
	AllowGroups          []string
}

// NewApp
//
//	@Description: Простое создание экземпляра приложения, использующее Echo.
//	@param cfg Конфигурация.
//	@return *gtserver.Server Echo сервер.
//	@return error Ошибка.
func NewApp(cfg NewAppCfg) (*gtserver.Server, error) {
	v := gtvalidate.Default()

	if err := v.Struct(cfg); err != nil {
		return nil, err
	}

	// #################################
	// ### Создание экземпляра Echo.
	// #################################
	e, err := New(cfg.EchoConfig)
	if err != nil {
		return nil, err
	}

	// #################################
	// ### Регистрация API модулей.
	// #################################
	err = DeclareModules(RegisterModulesConfig{
		Echo:             e.Instance,
		Modules:          cfg.Modules,
		AppName:          cfg.AppName,
		GlobalPrefix:     cfg.GlobalPrefix,
		PrintRoutes:      cfg.PrintRoutes,
		Logger:           cfg.DeclareModulesLogger,
		RoutesLogOneLine: cfg.RoutesLogOneLine,
		AllowGroups:      cfg.AllowGroups,
	})
	if err != nil {
		return nil, err
	}

	// #################################
	// ### Создание сервера.
	// #################################
	s, err := gtserver.New(gtserver.Config{
		Handler: e.Instance,
		Port:    cfg.Port,
		AppName: cfg.AppName,
		Logger:  cfg.ServerLogger,
	})
	if err != nil {
		return nil, err
	}

	return s, nil
}

// PrintRoutes выводит зарегистрированные роуты.
func PrintRoutes(e *echo.Echo) {
	log.Printf("\n\nRegistered routes:\n%s\n", strings.Repeat("-", 18))

	routes := e.Routes()
	for _, route := range routes {
		log.Printf("%-5s %s --> %s", route.Method, route.Path, stripPackage(route.Name))
	}
}

func stripPackage(n string) string {
	slashI := strings.LastIndex(n, "/")
	if slashI == -1 {
		slashI = 0 // для встроенных проектов.
	}
	return n[slashI+1:]
}
