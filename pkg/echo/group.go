package gtecho

// ToGroup объединить несколько роутов в одну группу.
func ToGroup(group string, routes []Route) []Route {
	for i := range routes {
		routes[i].Group = group
	}
	return routes
}
