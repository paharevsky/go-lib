package gtecho

const (
	MsgOk                  = "success"
	MsgCreated             = "created"
	MsgInternalServerError = "internal server error"
	MsgDeleted             = "deleted"
	MsgValidationError     = "validation error"
	MsgUnknownError        = "unknown error"
	MsgUnauthorized        = "unauthorized"
	MsgUpdated             = "updated"
)

type JResponse struct {
	Message any `json:"message"`
	Error   any `json:"error"`
	Payload any `json:"payload"`
}

// JMsg
//
//	@Description: Перевести сообщение в формат JSON.
//	@param message Сообщение.
//	@return J JSON с сообщением.
func JMsg(message any) J {
	return J{
		"message": message,
		"error":   nil,
		"payload": nil,
	}
}

// JErr
//
//	@Description: Перевести сообщение и ошибку в формат JSON.
//	@param err Ошибка.
//	@return J JSON с сообщением и ошибкой.
func JErr(message any, err any) J {
	return J{
		"message": message,
		"error":   err,
		"payload": nil,
	}
}

// JMsgErr Одинаковое содержимое сообщения и ошибки.
func JMsgErr(value any) J {
	return J{
		"message": value,
		"error":   value,
		"payload": nil,
	}
}

// JPayload
//
//	@Description: Перевести полезную нагрузку в формат JSON.
//	@param payload Полезная нагрузка (например, какие-либо данные о созданном ресурсе).
//	@return J JSON с полезной нагрузкой.
func JPayload(message any, payload any) J {
	return J{
		"message": message,
		"error":   nil,
		"payload": payload,
	}
}

// JOk Отправить успешный ответ с полезной нагрузкой.
func JOk(payload any) J {
	return J{
		"message": MsgOk,
		"error":   nil,
		"payload": payload,
	}
}

// JRes
//
//	@Description: Перевести полноформатный ответ от сервера в формат JSON.
//	@param message Сообщение.
//	@param payload Полезная нагрузка (например, какие-либо данные о созданном ресурсе).
//	@param err Ошибка.
//	@return J JSON с полноформатным ответом.
func JRes(message any, payload any, err any) J {
	return J{
		"message": message,
		"error":   err,
		"payload": payload,
	}
}
