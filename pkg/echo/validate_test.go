package gtecho

import (
	"bytes"
	"encoding/json"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/require"
	gtvalidate "gitlab.com/paharevsky/go-lib/pkg/validate"
	"net/http"
	"net/http/httptest"
	"testing"
)

var jb, _ = json.Marshal(TestStruct{"test"})

type TestStruct struct {
	Test string `json:"test"`
}

var TestValidateDto = []struct {
	name        string
	req         *http.Request
	validations []SimpleValidator
	errorReq    bool
}{
	{"Ok", httptest.NewRequest(echo.POST, "http://localhost:8080", bytes.NewReader(jb)), []SimpleValidator{"struct {test string `json:test`}", "notEmpty"}, false},
	{"Bad body", httptest.NewRequest(echo.GET, "http://localhost:8080", bytes.NewReader([]byte{1, 2, 3})), []SimpleValidator{""}, true},
	{"Nil validator", httptest.NewRequest(echo.GET, "http://localhost:8080", nil), []SimpleValidator{""}, true},
}

func TestEcho_ValidateDto(t *testing.T) {
	var (
		e  = echo.New()
		rr = httptest.NewRecorder()
	)
	e.Validator = &EchoValidator{Core: gtvalidate.Default()}

	for _, test := range TestValidateDto {
		if test.name == "Nil validator" {
			e.Validator = nil
		}
		test.req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		ctx := e.NewContext(test.req, rr)
		_, _, err := ValidateDto[TestStruct](ctx, test.validations...)
		if test.errorReq {
			require.Error(t, err, test.name)
		} else {
			require.Nil(t, err, test.name)
		}
		rr.Flush()
	}
}

var jbs, _ = json.Marshal([]TestStruct{{Test: "test"}})

var TestValidateArrDto = []struct {
	name        string
	req         *http.Request
	validations []SimpleValidator
	errorReq    bool
}{
	{"Ok", httptest.NewRequest(echo.POST, "http://localhost:8080", bytes.NewReader(jbs)), []SimpleValidator{"struct {test string `json:test`}", "notEmpty"}, false},
	{"Bad body", httptest.NewRequest(echo.GET, "http://localhost:8080", bytes.NewReader([]byte{1, 2, 3})), []SimpleValidator{""}, true},
	{"Nil validator", httptest.NewRequest(echo.GET, "http://localhost:8080", nil), []SimpleValidator{""}, true},
}

func TestEcho_ValidateArrDto(t *testing.T) {
	var (
		e  = echo.New()
		rr = httptest.NewRecorder()
	)
	e.Validator = &EchoValidator{Core: gtvalidate.Default()}

	for _, test := range TestValidateArrDto {
		if test.name == "Nil validator" {
			e.Validator = nil
		}
		test.req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		ctx := e.NewContext(test.req, rr)
		_, _, err := ValidateArrDto[TestStruct](ctx, test.validations...)
		if test.errorReq {
			require.Error(t, err, test.name)
		} else {
			require.Nil(t, err, test.name)
		}
		rr.Flush()
	}
}

func TestEcho_isNotEmptyStruct(t *testing.T) {
	err := isNotEmptyStruct[struct{}](nil)
	require.Error(t, err, "Test nil SLice")
}

func TestEcho_isNotEmptySlice(t *testing.T) {
	err := isNotEmptySlice[string](nil)
	require.Error(t, err, "Test nil SLice")

	err = isNotEmptySlice[string](&[]string{})
	require.Error(t, err, "Test empty SLice")
}
