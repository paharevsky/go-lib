package gtecho

import (
	"github.com/stretchr/testify/require"
	"testing"
)

var testMessage = "test"

func TestEcho_JsonMsg(t *testing.T) {
	jmsg := JMsg(testMessage)
	require.Equal(t, testMessage, jmsg["message"], "Expect msg match")
}
