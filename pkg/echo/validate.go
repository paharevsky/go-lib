package gtecho

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"net/http"
)

type SimpleValidator string

// ValidatorNotEmpty Не пустой слайс или структура.
const ValidatorNotEmpty SimpleValidator = "notEmpty"

// successStatusCode HTTP статус-код по-умолчанию.
const successStatusCode = 200

// isNotEmptySlice[T any]
//
//	@Description: Проверить слайс на пустоту.
//	@param slice Слайс.
//	@return error Ошибка.
func isNotEmptySlice[T any](slice *[]T) error {
	if slice == nil {
		return fmt.Errorf("slice is nil")
	}
	if len(*slice) == 0 {
		return fmt.Errorf("slice is empty")
	}
	return nil
}

// isNotEmptyStruct[T any]
//
//	@Description: Проверить структуру на пустоту.
//	@param arg Структура.
//	@return error Ошибка.
func isNotEmptyStruct[T any](arg *T) error {
	if arg == nil {
		return fmt.Errorf("struct is nil")
	}
	return nil
}

// ValidateArrDto ValidateArrDto[T any]
//
//	@Description: Проверить dto массива.
//	@param ec Echo Context.
//	@param validations Дополнительные параметры валидации.
//	@return *[]T DTO Массив.
//	@return StatusCode HTTP статус-код.
//	@return error Ошибка.
func ValidateArrDto[T any](ec echo.Context, validations ...SimpleValidator) (*[]T, int, error) {
	body := make([]T, 0)

	if err := ec.Bind(&body); err != nil {
		return nil, http.StatusBadRequest, err
	}

	if err := ec.Validate(body); err != nil {
		return nil, http.StatusBadRequest, err
	}

	if validations != nil && len(validations) != 0 {
		var err error

		for _, validation := range validations {
			switch validation {
			case ValidatorNotEmpty:
				err = isNotEmptySlice[T](&body)
			default:
				err = nil
			}

			if err != nil {
				return nil, http.StatusBadRequest, err
			}
		}
	}

	return &body, successStatusCode, nil
}

// ValidateDto ValidateDto[T any]
//
//	@Description: Проверить dto структуры.
//	@param ec Echo Context.
//	@param validations Дополнительные параметры валидации.
//	@return *[]T DTO структура.
//	@return StatusCode HTTP статус-код.
//	@return error Ошибка.
func ValidateDto[T any](ec echo.Context, validations ...SimpleValidator) (*T, int, error) {
	body := new(T)

	if err := ec.Bind(body); err != nil {
		return nil, http.StatusBadRequest, err
	}

	if err := ec.Validate(*body); err != nil {
		return nil, http.StatusBadRequest, err
	}

	if validations != nil && len(validations) != 0 {
		var err error

		for _, validation := range validations {
			switch validation {
			case ValidatorNotEmpty:
				err = isNotEmptyStruct[T](body)

			default:
				err = nil
			}

			if err != nil {
				return nil, http.StatusBadRequest, err
			}
		}
	}

	return body, successStatusCode, nil
}
