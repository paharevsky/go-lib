package gtecho

import (
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	gtvalidate "gitlab.com/paharevsky/go-lib/pkg/validate"
	"testing"
)

var logger = log.New()

var apiModules = func(e *echo.Echo) []Module {
	return []Module{}
}

type TestModule struct {
}

func (t TestModule) Routes() []Route {
	rts := []Route{}
	rt1 := Route{
		Method:                "GET",
		Path:                  "/test1",
		Handler:               nil,
		Middleware:            nil,
		RewriteGlobalPrefixTo: "",
		DisableGlobalPrefix:   false,
	}
	rt2 := Route{
		Method:                "POST",
		Path:                  "/test2",
		Handler:               nil,
		Middleware:            nil,
		RewriteGlobalPrefixTo: "some",
		DisableGlobalPrefix:   false,
	}
	rt3 := Route{
		Method:                "PUT",
		Path:                  "/test3",
		Handler:               nil,
		Middleware:            nil,
		RewriteGlobalPrefixTo: "",
		DisableGlobalPrefix:   true,
	}
	rt4 := Route{
		Method:                "DELETE",
		Path:                  "/test1",
		Handler:               nil,
		Middleware:            nil,
		RewriteGlobalPrefixTo: "",
		DisableGlobalPrefix:   false,
	}
	rt5 := Route{
		Method:                "PATCH",
		Path:                  "/test1",
		Handler:               nil,
		Middleware:            nil,
		RewriteGlobalPrefixTo: "",
		DisableGlobalPrefix:   false,
	}
	rt6 := Route{
		Method:                "HEAD",
		Path:                  "/test1",
		Handler:               nil,
		Middleware:            nil,
		RewriteGlobalPrefixTo: "",
		DisableGlobalPrefix:   false,
	}
	rts = append(rts, rt1, rt2, rt3, rt4, rt5, rt6)
	return rts
}

var testModules = func(e *echo.Echo) (mds []Module) {
	var a TestModule
	mds = append(mds, a)
	return
}

var testValidate = &EchoValidator{
	Core: validator.New(),
}

var TestNewApp = []struct {
	name     string
	cfg      NewAppCfg
	errorReq bool
}{
	{"Ok", NewAppCfg{
		AppName:              "test-app",
		GlobalPrefix:         "api",
		Port:                 1234,
		Modules:              testModules,
		DeclareModulesLogger: logger,
		ServerLogger:         logger,
		PrintRoutes:          true,
		EchoConfig:           EchoConfig{},
	}, false},
	{"Not nil validator", NewAppCfg{
		AppName:      "test-app",
		GlobalPrefix: "api",
		Port:         1234,
		Modules:      apiModules,
		ServerLogger: logger,
		PrintRoutes:  true,
		EchoConfig:   EchoConfig{Validator: testValidate},
	}, false},
	{"Nil apiModules", NewAppCfg{
		AppName:      "test-app",
		GlobalPrefix: "api",
		Port:         1234,
		Modules:      nil,
		ServerLogger: logger,
		PrintRoutes:  true,
		EchoConfig:   EchoConfig{Validator: testValidate},
	}, true},
}

func TestEcho_NewApp(t *testing.T) {
	for _, test := range TestNewApp {
		_, err := NewApp(test.cfg)
		if test.errorReq {
			require.Errorf(t, err, "In test %s expected error", test.name)
		} else {
			require.Nilf(t, err, "In test %s unexpected error: %s", test.name, err)
		}
	}
}

var TestApiRoutes = []struct {
	name     string
	cfg      RegisterModulesConfig
	errorReq bool
}{
	{"Ok", RegisterModulesConfig{
		Echo:         echo.New(),
		Modules:      apiModules,
		AppName:      "Test",
		GlobalPrefix: "api",
		PrintRoutes:  false,
	}, false},
	{"Empty fn modules", RegisterModulesConfig{
		Echo:         echo.New(),
		Modules:      nil,
		AppName:      "Test",
		GlobalPrefix: "api",
		PrintRoutes:  false,
	}, true},
	{"Empty service name", RegisterModulesConfig{
		Echo:         echo.New(),
		Modules:      apiModules,
		AppName:      "",
		GlobalPrefix: "api",
		PrintRoutes:  false,
	}, true},
	{"Empty echo cfg", RegisterModulesConfig{
		Echo:         nil,
		Modules:      apiModules,
		AppName:      "Test",
		GlobalPrefix: "api",
		PrintRoutes:  false,
	}, true},
	{"Test modules", RegisterModulesConfig{
		Echo:         echo.New(),
		Modules:      testModules,
		AppName:      "Test",
		GlobalPrefix: "api",
		PrintRoutes:  true,
	}, false},
}

func TestEcho_DefineEchoApiRoutes(t *testing.T) {
	for _, test := range TestApiRoutes {
		err := DeclareModules(test.cfg)
		if test.errorReq {
			require.Errorf(t, err, "In test %s expected error", test.name)
		} else {
			require.Nilf(t, err, "In test %s unexpected error: %s", test.name, err)
		}
	}
}

func TestEcho_EchoPrintRoutes(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Errorf("In test %s unexpected panic", "EchoPrintRoutes")
		}
	}()
	err := DeclareModules(TestApiRoutes[4].cfg)
	require.Nilf(t, err, "In test %s unexpected error: %s", "EchoPrintRoutes", err)
	EchoPrintRoutes(TestApiRoutes[4].cfg.Echo)
}

func TestEcho_EchoLoggerMiddleware(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Errorf("In test %s unexpected panic", "EchoLoggerMiddleware")
		}
	}()
	err := DeclareModules(TestApiRoutes[4].cfg)
	require.Nilf(t, err, "In test %s unexpected error: %s", "EchoLoggerMiddleware", err)
	EchoLoggerMiddleware(TestApiRoutes[4].cfg.Echo)
}

func myValidate(sl validator.StructLevel) {
	if sl.Current().Interface().(TestChildStruct).Value > 0 {
		sl.ReportError(sl.Current().Interface(), "TestChildStruct", "", "", "")
	}
}

type TestChildStruct struct {
	Value int
}

type TestParentStruct struct {
	Child TestChildStruct
}

func TestEcho_Validate(t *testing.T) {
	testValidator := EchoValidator{Core: gtvalidate.Default()}
	err := testValidator.Validate([]int{1, 2, 3})
	require.Nilf(t, err, "In test unexpected error: %s", "Ok int slice", err)

	err = testValidator.Validate(struct {
	}{})
	require.Nilf(t, err, "In test unexpected error: %s", "Ok strucr", err)
	testValidator.Core.RegisterStructValidation(myValidate, TestChildStruct{})
	data := &TestParentStruct{
		Child: TestChildStruct{
			Value: 20,
		},
	}
	err = testValidator.Validate(data)
	require.Error(t, err, "Validate test expect error")
	fmt.Println(err)
}
