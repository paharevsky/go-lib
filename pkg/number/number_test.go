package gtnum

import (
	"github.com/stretchr/testify/require"
	"testing"
)

var TestMaxOf = []struct {
	name        string
	values      any
	reqMax      any
	errRequired bool
}{
	{"Ints", []int64{-1, 2, 5, 12, 2, 12, -2}, int64(12), false},
	{"Floats", []float32{-1.1, 2.2, 5.5, 12.1, 2.2, 12.3, -2.1}, float32(12.3), false},
	{"Empty Slice", []int64{}, "", true},
	{"Nil Slice", nil, "", true},
}

func TestNumber_MaxOf(t *testing.T) {
	var (
		max any
		err error
	)
	for _, test := range TestMaxOf {
		switch test.values.(type) {
		case []int64:
			max, err = MaxOf(test.values.([]int64)...)
			if !test.errRequired {
				require.Equalf(t, test.reqMax.(int64), max.(int64), "In test %s, unexpected max value, required %v, have %v", test.name, test.reqMax, max)
			}
		case []float32:
			max, err = MaxOf(test.values.([]float32)...)
			if !test.errRequired {
				require.Equalf(t, test.reqMax.(float32), max.(float32), "In test %s, unexpected max value, required %v, have %v", test.name, test.reqMax, max)
			}
		case nil:
			sl := []int64{}
			sl = nil
			_, err = MaxOf(sl...)
		}
		if !test.errRequired {
			require.Nilf(t, err, "In test %s, unexpected error: %s", test.name, err)
		} else {
			require.Errorf(t, err, "In test %s, expected error bot don't have one", test.name)
		}
	}
}

var TestMinOf = []struct {
	name        string
	values      any
	reqMax      any
	errRequired bool
}{
	{"Ints", []int64{-1, 2, 5, 12, 2, 12, -2}, int64(-2), false},
	{"Floats", []float32{-1.1, 2.2, 5.5, 12.1, 2.2, 12.3, -2.1}, float32(-2.1), false},
	{"Empty Slice", []int64{}, "", true},
	{"Nil Slice", nil, "", true},
}

func TestNumber_MinOf(t *testing.T) {
	var (
		max any
		err error
	)
	for _, test := range TestMinOf {
		switch test.values.(type) {
		case []int64:
			max, err = MinOf(test.values.([]int64)...)
			if !test.errRequired {
				require.Equalf(t, test.reqMax.(int64), max.(int64), "In test %s, unexpected max value, required %v, have %v", test.name, test.reqMax, max)
			}
		case []float32:
			max, err = MinOf(test.values.([]float32)...)
			if !test.errRequired {
				require.Equalf(t, test.reqMax.(float32), max.(float32), "In test %s, unexpected max value, required %v, have %v", test.name, test.reqMax, max)
			}
		case nil:
			sl := []int64{}
			sl = nil
			_, err = MinOf(sl...)
		}
		if !test.errRequired {
			require.Nilf(t, err, "In test %s, unexpected error: %s", test.name, err)
		} else if err == nil && test.errRequired {
			require.Errorf(t, err, "In test %s, expected error bot don't have one", test.name)
		}
	}
}
