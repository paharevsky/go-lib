package gtnum

import (
	"errors"
)

var ErrNumsIsNil = errors.New("nums is nil")
var ErrNumsIsEmpty = errors.New("nums is empty")

type DefaultValueType int

const DefaultValue = 0

type Number interface {
	int | int8 | int16 | int32 | int64 | uint | uint8 | uint16 | uint32 | uint64 | float32 | float64
}

// Max
//
//	@Description:
//	@param a
//	@param b
//	@return int
func Max[T Number](a, b T) T {
	if a > b {
		return a
	}
	return b
}

// Min
//
//	@Description:
//	@param a
//	@param b
//	@return int
func Min[T Number](a, b T) T {
	if a < b {
		return a
	}
	return b
}

// MaxOf
//
//	@Description:
//	@param nums
//	@return int
//	@return error
func MaxOf[T Number](nums ...T) (T, error) {
	if nums == nil {
		return DefaultValue, ErrNumsIsNil
	}

	if len(nums) == 0 {
		return DefaultValue, ErrNumsIsEmpty
	}

	max := nums[0]

	for i := 1; i < len(nums); i++ {
		max = Max[T](max, nums[i])
	}

	return max, nil
}

// MinOf
//
//	@Description:
//	@param nums
//	@return int
//	@return error
func MinOf[T Number](nums ...T) (T, error) {
	if nums == nil {
		return DefaultValue, ErrNumsIsNil
	}

	if len(nums) == 0 {
		return DefaultValue, ErrNumsIsEmpty
	}

	max := nums[0]

	for i := 1; i < len(nums); i++ {
		max = Min[T](max, nums[i])
	}

	return max, nil
}
