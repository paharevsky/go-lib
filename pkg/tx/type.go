package gttx

import (
	"context"
	trm "gitlab.com/paharevsky/go-lib/pkg/tx/internal/core"
	"gitlab.com/paharevsky/go-lib/pkg/tx/internal/core/manager"
)

type Manager interface {
	Do(ctx context.Context, fn func(ctx context.Context) error) (err error)
	DoWithSettings(ctx context.Context, s trm.Settings, fn func(ctx context.Context) error) (err error)
	Init(ctx context.Context, s trm.Settings) (context.Context, manager.Closer, error)
}
