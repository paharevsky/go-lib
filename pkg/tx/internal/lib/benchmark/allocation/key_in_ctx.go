package main

import (
	"context"
	"sync"

	"gitlab.com/paharevsky/go-lib/pkg/tx/internal/lib/benchmark/common"
)

func keyInContext() {
	ctx := context.Background()

	key := common.CtxKey{}
	idKey := common.IDKey(1)
	ctx = context.WithValue(ctx, key, idKey)

	wg := sync.WaitGroup{}

	wg.Add(1)

	go nestedKeyInContext(ctx, &wg)

	wg.Wait()
}

func nestedKeyInContext(_ context.Context, wg *sync.WaitGroup) {
	wg.Done()
}
