package gtsqlx

import (
	"context"
	"gitlab.com/paharevsky/go-lib/pkg/tx/driver/sql"

	"github.com/jmoiron/sqlx"

	"gitlab.com/paharevsky/go-lib/pkg/tx/internal/core"
)

// NewDefaultFactory creates default trm.Transaction(sqlx.Tx).
func NewDefaultFactory(db *sqlx.DB) trm.TrFactory {
	return NewFactory(db, gtsql.NewSavePoint())
}

// NewFactory creates trm.Transaction(sql.Tx).
func NewFactory(db *sqlx.DB, sp gtsql.SavePoint) trm.TrFactory {
	return func(ctx context.Context, trms trm.Settings) (context.Context, trm.Transaction, error) {
		s, _ := trms.(gtsql.Settings)

		return NewTransaction(ctx, sp, s.TxOpts(), db)
	}
}
