package gtsqlx

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/paharevsky/go-lib/pkg/tx"
	"gitlab.com/paharevsky/go-lib/pkg/tx/internal/core/manager"
)

// NewPgxV5Manager creates a new pgxv5 transaction manager.
func NewPgxV5Manager(db *sqlx.DB) gttx.Manager {
	return manager.Must(NewDefaultFactory(db))
}
