//go:build go1.19
// +build go1.19

package gtpgxv5

import (
	"context"

	"gitlab.com/paharevsky/go-lib/pkg/tx/internal/core"
)

// NewDefaultFactory creates default trm.Transaction(pgx.Tx).
func NewDefaultFactory(db Transactional) trm.TrFactory {
	return NewFactory(db)
}

// NewFactory creates trm.Transaction(pgx.Tx).
func NewFactory(db Transactional) trm.TrFactory {
	return func(ctx context.Context, trms trm.Settings) (context.Context, trm.Transaction, error) {
		s, _ := trms.(Settings)

		return NewTransaction(ctx, s.TxOpts(), db)
	}
}
