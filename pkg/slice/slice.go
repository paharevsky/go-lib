package gtslice

// Index возвращает индекс первого вхождения v в s, или -1, если его нет.
func Index[S ~[]E, E comparable](slice S, value E) int {
	for i := range slice {
		if value == slice[i] {
			return i
		}
	}
	return -1
}

// IndexFunc возвращает первый индекс i, удовлетворяющий f(s[i]), или -1,
// если ни один из них не удовлетворяет.
func IndexFunc[S ~[]E, E any](slice S, f func(E) bool) int {
	for i := range slice {
		if f(slice[i]) {
			return i
		}
	}
	return -1
}

// Contains присутствует ли v в s.
func Contains[S ~[]E, E comparable](slice S, value E) bool {
	return Index(slice, value) >= 0
}

// ContainsFunc удовлетворяет ли хотя бы один
// элемент e из s f(e).
func ContainsFunc[S ~[]E, E any](slice S, f func(E) bool) bool {
	return IndexFunc(slice, f) >= 0
}

// Concat объединяет несколько слайсов в один и возвращает его.
func Concat[T any](slices [][]T) []T {
	var totalLen int

	for _, s := range slices {
		totalLen += len(s)
	}

	result := make([]T, totalLen)

	var i int

	for _, s := range slices {
		i += copy(result[i:], s)
	}

	return result
}
