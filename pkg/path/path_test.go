package gtpath

import (
	"github.com/stretchr/testify/require"
	"strings"
	"testing"
)

var filename string

func TestPath_Filename(t *testing.T) {
	filename = Filename()
	nameSlice := strings.Split(filename, "/")
	require.Equalf(t, "path_test.go", nameSlice[len(nameSlice)-1], "In test %s invalid file name", "Ok Filename")
	require.Equalf(t, "path", nameSlice[len(nameSlice)-2], "In test %s invalid directory name", "Ok Filename")
}

func TestPath_ReplaceLast(t *testing.T) {
	newFileName := ReplaceLast(filename, "/path.go")
	newNameSlice := strings.Split(newFileName, "/")
	require.Equalf(t, "path.go", newNameSlice[len(newNameSlice)-1], "In test %s invalid file name replacement", "Ok ReplaceLast")
	emptyFileName := ReplaceLast("", "/path_test.go")
	require.Equalf(t, "/path_test.go", emptyFileName, "In test %s invalid empty path insertion %v", "Ok ReplaceLast empty path", emptyFileName)

}
