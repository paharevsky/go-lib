// Package gtpath
// @Description: Инструменты для работы с путями.
package gtpath

import (
	"runtime"
	"strings"
)

// Filename
//
//	@Description: Эквивалент  __filename в NodeJS.
//	@return string Путь до файла, включая сам файл.
func Filename() string {
	_, filename, _, _ := runtime.Caller(1)
	return filename
}

// ReplaceLast
//
//	@Description: Заменить последний элемент в пути.
//	@param path Путь.
//	@param newLast Новый конец пути.
//	@return string Обновленный путь.
func ReplaceLast(path string, newLast string) string {
	split := strings.Split(path, "/")

	if len(split) <= 1 {
		return newLast
	}

	path = strings.Join(split[:len(split)-1], "/") + newLast

	return path
}
