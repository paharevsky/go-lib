// Package gtplenty
// @Description: Инструменты для работы с массивом, слайсом и мапой.
package gtplenty

// SliceIsNotEmpty SliceIsNotEmpty[T comparable]
//
//	@Description: Проверить слайс на пустоту.
//	@param slice Слайс.
//	@return bool true-не пустой, false-пустой.
func SliceIsNotEmpty[T any](slice []T) bool {
	if slice == nil || len(slice) == 0 {
		return false
	}

	return true
}

// SliceIsEmpty SliceIsEmpty[T comparable]
//
//	@Description: Проверить слайс на пустоту.
//	@param slice Слайс.
//	@return bool true-пустой, false-не пустой.
func SliceIsEmpty[T any](slice []T) bool {
	return !SliceIsNotEmpty[T](slice)
}

// MapIsNotEmpty MapIsNotEmpty[K comparable, V any]
//
//	@Description: Проверить мапу на пустоту.
//	@param m Мапа.
//	@return bool true-не пустая, false-пустая.
func MapIsNotEmpty[K comparable, V any](m map[K]V) bool {
	if m == nil || len(m) == 0 {
		return false
	}

	return true
}

// MapIsEmpty MapIsEmpty[K comparable, V any]
//
//	@Description: Проверить мапу на пустоту.
//	@param m Мапа.
//	@return bool true-пустая, false-не пустая.
func MapIsEmpty[K comparable, V any](m map[K]V) bool {
	return !MapIsNotEmpty[K, V](m)
}
