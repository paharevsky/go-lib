package gtplenty

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestPlenty_SliceNotEmpty(t *testing.T) {
	var someSlice []any
	require.False(t, SliceIsNotEmpty(someSlice), "Nil slice should be empty")

	someSlice = make([]any, 0)
	require.False(t, SliceIsNotEmpty(someSlice), "Empty slice should be empty")

	someSlice = append(someSlice, "some")
	require.True(t, SliceIsNotEmpty(someSlice), "Non empty slice shouldn't be empty")

}

func TestPlenty_SliceIsEmpty(t *testing.T) {
	var someSlice []any
	require.True(t, SliceIsEmpty(someSlice), "Nil slice should be empty")

	someSlice = make([]any, 0)
	require.True(t, SliceIsEmpty(someSlice), "Empty slice should be empty")

	someSlice = append(someSlice, "some")
	require.False(t, SliceIsEmpty(someSlice), "Non empty slice shouldn't be empty")

}

func TestPlenty_MapIsNotEmpty(t *testing.T) {
	var someMap map[any]struct{}
	require.False(t, MapIsNotEmpty(someMap), "Nil map should be empty")

	someMap = make(map[any]struct{})
	require.False(t, MapIsNotEmpty(someMap), "Empty map should be empty")

	someMap["some"] = struct{}{}
	require.True(t, MapIsNotEmpty(someMap), "Non empty map shouldn't be empty")

}

func TestPlenty_MapIsEmpty(t *testing.T) {
	var someMap map[any]struct{}
	require.True(t, MapIsEmpty(someMap), "Nil map should be empty")

	someMap = make(map[any]struct{})
	require.True(t, MapIsEmpty(someMap), "Empty map should be empty")

	someMap["some"] = struct{}{}
	require.False(t, MapIsEmpty(someMap), "Non empty map shouldn't be empty")

}
