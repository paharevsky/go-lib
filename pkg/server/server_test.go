package gtserver

import (
	"context"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	"net/http"
	"testing"
	"time"
)

var cfg = Config{}

type handler struct {
}

func (h handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

}

var TestNewServer = []struct {
	name        string
	appName     string
	handler     http.Handler
	logger      *log.Logger
	expectedErr bool
}{
	{"Empty cfg", "", nil, nil, true},
	{"With name cfg", "Some App", nil, nil, true},
	{"With name and handler cfg", "Some App", handler{}, nil, false},
	{"With all necessary args cfg", "Some App", handler{}, log.New(), false},
}

func TestServer_NewServer(t *testing.T) {
	for _, test := range TestNewServer {
		cfg.AppName = test.appName
		cfg.Handler = test.handler
		cfg.Logger = test.logger
		srv, err := New(cfg)
		if err != nil && !test.expectedErr {
			t.Errorf("At test %s unexpected error: %s", test.name, err)
		} else if err == nil && test.expectedErr {
			t.Errorf("At test %s expected error but it's nil", test.name)
		} else if err == nil && srv == nil {
			t.Errorf("At test %s unexpected behaviour, server == nil", test.name)
		}
	}
}

func TestServer_Run(t *testing.T) {
	srv, err := New(cfg)
	if err != nil {
		t.Error("Unexpected error while creating server")
	}

	go func(err error) {
		err = srv.Run()
	}(err)

	require.Nil(t, err, "Unexpected error while running server")

	time.Sleep(time.Second / 2)

	err = srv.Shutdown(context.Background())
	require.Nil(t, err, "Unexpected error while shutting down server")
}
