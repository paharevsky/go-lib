// Package gtserver
// @Description: Инструменты для работы с http-сервером.
package gtserver

import (
	"context"
	"crypto/tls"
	"fmt"
	log "github.com/sirupsen/logrus"
	defaultLog "log"
	"net"
	"net/http"
	"time"
)

// Server
// @Description: Структура сервера приложения.
type Server struct {
	httpServer *http.Server
	appName    string
	logger     *log.Logger
}

// Config
// @Description: Структура конфигурации сервера приложения.
type Config struct {
	Addr                         string
	Handler                      http.Handler
	DisableGeneralOptionsHandler bool
	TLSConfig                    *tls.Config
	ReadTimeout                  time.Duration
	ReadHeaderTimeout            time.Duration
	WriteTimeout                 time.Duration
	IdleTimeout                  time.Duration
	MaxHeaderBytes               int
	TLSNextProto                 map[string]func(*http.Server, *tls.Conn, http.Handler)
	ConnState                    func(net.Conn, http.ConnState)
	ErrorLog                     *defaultLog.Logger
	BaseContext                  func(net.Listener) context.Context
	ConnContext                  func(ctx context.Context, c net.Conn) context.Context
	Port                         uint
	AppName                      string
	Logger                       *log.Logger
}

// New
//
//	@Description: Создание нового сервера приложения.
//	@param cfg Конфигурация конструктора.
//	@return *Server Экземпляр объекта сервера.
//	@return error
func New(cfg Config) (*Server, error) {
	if cfg.AppName == "" {
		return nil, fmt.Errorf("app name is required")
	}

	if cfg.Handler == nil {
		return nil, fmt.Errorf("handler is required")
	}

	if cfg.Logger == nil {
		cfg.Logger = log.New()
	}

	if cfg.Port == 0 {
		cfg.Port = 4500
	}

	if cfg.ReadTimeout == 0 {
		cfg.ReadTimeout = 10 * time.Second
	}

	if cfg.WriteTimeout == 0 {
		cfg.WriteTimeout = 10 * time.Second
	}

	if cfg.MaxHeaderBytes == 0 {
		cfg.MaxHeaderBytes = 1 << 20 // 1 MB
	}

	addr := fmt.Sprintf(":%v", cfg.Port)

	httpServer := &http.Server{
		Addr:                         addr,
		Handler:                      cfg.Handler,
		DisableGeneralOptionsHandler: cfg.DisableGeneralOptionsHandler,
		TLSConfig:                    cfg.TLSConfig,
		ReadTimeout:                  cfg.ReadTimeout,
		ReadHeaderTimeout:            cfg.ReadHeaderTimeout,
		WriteTimeout:                 cfg.WriteTimeout,
		IdleTimeout:                  cfg.IdleTimeout,
		MaxHeaderBytes:               cfg.MaxHeaderBytes,
		TLSNextProto:                 cfg.TLSNextProto,
		ConnState:                    cfg.ConnState,
		ErrorLog:                     cfg.ErrorLog,
		BaseContext:                  cfg.BaseContext,
		ConnContext:                  cfg.ConnContext,
	}

	return &Server{
		httpServer: httpServer,
		appName:    cfg.AppName,
		logger:     cfg.Logger,
	}, nil
}

// Run
//
//	@Description: Запуск сервера приложения.
//	@receiver s
//	@return error
func (s *Server) Run() error {
	s.logger.Println("[" + s.appName + "] starting server on http://localhost" + s.httpServer.Addr)
	err := s.httpServer.ListenAndServe()

	return err
}

// Shutdown
//
//	@Description: Остановка сервера.
//	@receiver s
//	@param ctx Внешний контекст.
//	@return error
func (s *Server) Shutdown(ctx context.Context) error {
	s.logger.Println("[" + s.appName + "] shutting down server on http://localhost" + s.httpServer.Addr)

	return s.httpServer.Shutdown(ctx)
}
