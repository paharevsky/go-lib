package gtset

import (
	"github.com/stretchr/testify/require"
	"reflect"
	"testing"
)

var set = New[any]()

func TestSet_New(t *testing.T) {
	if reflect.TypeOf(set).Kind() != reflect.TypeOf(map[any]struct{}{}).Kind() {
		t.Error("Invalid set type")
	}
}

var TestAdd = []struct {
	name string
	val  any
}{
	{"string", "some"},
	{"int", 1},
	{"bool", true},
	{"float", 1.1},
}

func TestSet_Add(t *testing.T) {

	for _, test := range TestAdd {
		set.Add(test.val)
		_, ok := set[test.val]
		require.Truef(t, ok, "Test add %s failed", test.name)
	}
}

var TestDelete = []struct {
	name string
	val  any
}{
	{"string", "some"},
	{"int", 1},
}

func TestSet_Delete(t *testing.T) {
	for _, test := range TestDelete {
		set.Delete(test.val)
		_, ok := set[test.val]
		require.Falsef(t, ok, "Test delete %s failed", test.name)
	}
}

var TestHas = []struct {
	name     string
	val      any
	expected bool
}{
	{"string", "some", false},
	{"int", 1, false},
	{"bool", true, true},
	{"float", 1.1, true},
}

func TestSet_Has(t *testing.T) {
	for _, test := range TestHas {
		require.Equalf(t, test.expected, set.Has(test.val), "Test has %s failed", test.name)
	}
}

func TestSet_Len(t *testing.T) {
	require.Equal(t, 2, set.Len(), "Invalid set len")
}

func TestSet_ForEach(t *testing.T) {
	set.ForEach(func(a any) {
		require.True(t, set.Has(a), "Invalid func integration for each")
	})
}

func TestSet_Clear(t *testing.T) {
	set.Clear()
	require.Equal(t, 0, set.Len(), "There are some values after clear function")
}
