// Package gtset
// @Description: Создание и управление множеством, содержащего уникальные элементы (Set).
package gtset

type Set[T comparable] map[T]struct{}

// New New[T comparable]
//
//	@Description: Создать множество (Set).
//	@return Set[T] Объект множества (Set).
func New[T comparable]() Set[T] {
	return make(Set[T])
}

// Add
//
//	@Description: Добавить значение.
//	@receiver s Set.
//	@param v Значение.
func (s Set[T]) Add(v T) {
	s[v] = struct{}{}
}

// Delete
//
//	@Description: Удалить значение.
//	@receiver s Set.
//	@param v Значение.
func (s Set[T]) Delete(v T) {
	delete(s, v)
}

// Has
//
//	@Description: Имеется ли значение в множестве.
//	@receiver s Set.
//	@param v Значение.
//	@return bool true-имеется, false-не имеется.
func (s Set[T]) Has(v T) bool {
	_, ok := s[v]
	return ok
}

// Len
//
//	@Description: Получить длину множества.
//	@receiver s Set.
//	@return int Длина множества.
func (s Set[T]) Len() int {
	return len(s)
}

// Clear
//
//	@Description: Очистить множество.
//	@receiver s Set.
func (s Set[T]) Clear() {
	for k := range s {
		delete(s, k)
	}
}

// ForEach
//
//	@Description: Итерация по всем элементам множества.
//	@receiver s Set.
//	@param f Callback.
func (s Set[T]) ForEach(f func(T)) {
	for v := range s {
		f(v)
	}
}
