package gtsse

import (
	"fmt"
	"github.com/stretchr/testify/require"
	"net/http/httptest"
	"testing"
	"time"
)

var hub *Hub[int, int]

func TestSSE_New(t *testing.T) {
	hub = New[int, int]()
	require.NotNil(t, hub, "unexpected nil hub created")
}

func TestSSE_SetResponseHeaders(t *testing.T) {
	rr := httptest.NewRecorder()
	SetResponseHeaders(rr)
	require.Equal(t, "text/event-stream", rr.Header().Get("Content-Type"), "invalid Content-Type header")
	require.Equal(t, "no-cache", rr.Header().Get("Cache-Control"), "invalid Cache-Control header")
	require.Equal(t, "keep-alive", rr.Header().Get("Connection"), "invalid Connection header")
}

func TestSSE_Listen(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Error("panicked while listening")
		}
	}()
	go hub.Listen()
	time.Sleep(time.Second)
	hub.closeConnection <- 1
}

func TestSSE_SetConnection(t *testing.T) {
	fmt.Println("SET CONNECTION")
	defer func() {
		if r := recover(); r != nil {
			t.Error("panicked while setting connection")
		}
	}()
	testChan := make(chan int)
	hub.SetConnection(0, testChan)
}

func TestSSE_GetConnections(t *testing.T) {
	connMap := hub.GetConnections()
	_, ok := connMap[0]
	require.True(t, ok, "can't get earlier set connection")
}

func TestSSE_CloseConnection(t *testing.T) {
	hub.CloseConnection(0)
	cc := hub.GetConnections()[0]
	_, ok := <-cc
	require.False(t, ok, "can't close earlier set connection")
}

//func TestSSE_NewSteam(t *testing.T) {
//	var (
//		rr  = httptest.NewRecorder()
//		req = httptest.NewRequest("GET", "/", nil)
//		err error
//	)
//
//	go func(err error) {
//		err = hub.NewSteam(rr, req, 0, func() int { return 1 }, func() { return })
//	}(err)
//	require.Nilf(t, err, "can't create new stream: %s", err)
//
//	time.Sleep(time.Second * 2)
//	err = hub.Send(0, 0)
//	require.Nilf(t, err, "unexpected error while sending: %s", err)
//
//	err = hub.Send(23, 1)
//	require.Error(t, err, "expected error while sending")
//
//	ctx, cancel := context.WithCancel(context.Background())
//	req = req.WithContext(ctx)
//	go func(err error) {
//		err = hub.NewSteam(rr, req, 2, func() int { return 0 }, func() { return })
//		require.Nilf(t, err, "can't create new stream %s", err)
//	}(err)
//
//	cancel()
//
//	<-req.Context().Done()
//}
