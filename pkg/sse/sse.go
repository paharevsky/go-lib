// Package gtsse
// @Description: Инструменты для работы с SSE.
package gtsse

import (
	"errors"
	"fmt"
	gtchan "gitlab.com/paharevsky/go-lib/pkg/chan"
	"net/http"
)

var ErrConnectionIsNotExist = errors.New("sse connection is not exist")
var ErrNotSupported = errors.New("sse streaming is not supported")

// Hub Hub[K comparable, V comparable]
// @Description: Структура для работы с SSE.
type Hub[K comparable, V comparable] struct {
	// Канал для приема новых подключений (connections)
	newConnection chan chan V

	// Канал для закрытия подключения (connections)
	closeConnection chan K

	// Подключения
	connections map[K]chan V
}

// New New[K comparable-тип ключей (id) соединений, V comparable-тип значения, которое передает соединение]
//
//	@Description: Конструктор объекта для работы с SSE.
//	@return *Hub[K, V] Объект для работы с SSE.
func New[K comparable, V comparable]() *Hub[K, V] {
	closeConnection := make(chan K)
	connections := make(map[K]chan V)

	return &Hub[K, V]{
		closeConnection: closeConnection,
		connections:     connections,
	}
}

// SetResponseHeaders
//
//	@Description: Установить заголовки HTTP-ответа для работы с SSE.
//	@param rw Объект ResponseWriter.
func SetResponseHeaders(rw http.ResponseWriter) {
	rw.Header().Set("Content-Type", "text/event-stream")
	rw.Header().Set("Cache-Control", "no-cache")
	rw.Header().Set("Connection", "keep-alive")
}

// Listen
//
//	@Description: Обслуживание соединений.
//	@receiver h
func (h *Hub[K, V]) Listen() {
	for {
		select {
		case connectionId := <-h.closeConnection:
			{
				delete(h.connections, connectionId)
			}
		}
	}
}

// GetConnections
//
//	@Description: Получить все соединения.
//	@receiver h
//	@return map[T]chan
func (h *Hub[K, V]) GetConnections() map[K]chan V {
	return h.connections
}

// SetConnection
//
//	@Description: Записать новое соединение в список соединений.
//	@receiver h
//	@param ch Канал, по которому установлено соединение.
func (h *Hub[K, V]) SetConnection(key K, ch chan V) {
	h.connections[key] = ch
}

// CloseConnection
//
//	@Description: Закрыть соединение
//	@receiver h
//	@param key Ключ (id) соединения.
func (h *Hub[K, V]) CloseConnection(key K) {
	h.closeConnection <- key
	close(h.connections[key])
}

// NewSteam
//
//	@Description: Начать SSE стрим.
//	@receiver h
//	@param w ResponseWriter.
//	@param r Request.
//	@param key Ключ (id) нового соединения.
//	@param setupFn Функция предустановки SSE-соединения.
//	@param serviceFn Функция обслуживания SSE-соединения.
//	@return error Ошибка.
func (h *Hub[K, V]) NewSteam(w http.ResponseWriter, r *http.Request, key K, setupFn func() V, serviceFn func()) error {
	isClosed := false
	flusher, ok := w.(http.Flusher)
	if !ok {
		return ErrNotSupported
	}

	SetResponseHeaders(w)
	clientConnection := make(chan V)
	h.SetConnection(key, clientConnection)

	isRequestClosed := r.Context().Done()

	go func() {
		<-isRequestClosed
		h.CloseConnection(key)
		isClosed = true
	}()

	go func() {
		if isClosed {
			return
		}

		var val V

		if setupFn != nil {
			val = setupFn()
		}

		if gtchan.IsClosed(clientConnection) {
			isClosed = true
			return
		}

		clientConnection <- val
	}()

	for {
		if isClosed {
			return nil
		}
		fmt.Fprintf(w, "data: %v\n\n", <-clientConnection)
		if serviceFn != nil {
			serviceFn()
		}
		flusher.Flush()
	}
}

// Send
//
//	@Description: Отправить данные абоненту, если он есть.
//	@receiver h
//	@param key Ключ (id) соединения.
//	@param value Данные.
//	@return error Ошибка.
func (h *Hub[K, V]) Send(key K, value V) error {
	if _, exist := h.connections[key]; exist == true {
		conn := h.connections[key]
		conn <- value
		return nil
	}
	return ErrConnectionIsNotExist
}
