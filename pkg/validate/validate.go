package gtvalidate

import "github.com/go-playground/validator/v10"

var validate *validator.Validate

func Default() *validator.Validate {
	if validate == nil {
		validate = validator.New(validator.WithRequiredStructEnabled())
	}
	return validate
}
