package gtdi

import (
	log "github.com/sirupsen/logrus"
	"io"
	"os"
)

type LoggerFormat string

var LoggerFormatJSON LoggerFormat = "json"

var LoggerFormatCommon LoggerFormat = "common"

type Logger *log.Logger

type LoggerConfig struct {
	Format LoggerFormat
}

// ProvideLogger
//
//	@Description: Зарегистрировать логгер в di контейнере.
//	@param cfg Конфигурация логгера.
//	@return error Ошибка.
func ProvideLogger(cfg LoggerConfig) error {
	return providePlugin[Logger](func() (Logger, error) {
		logger := log.New()

		if cfg.Format == LoggerFormatJSON {
			logger.Formatter = &log.JSONFormatter{}
			logger.SetLevel(log.DebugLevel)
			log.SetOutput(logger.Writer())
			logger.SetOutput(io.MultiWriter(os.Stdout))
		}

		return logger, nil
	})
}
