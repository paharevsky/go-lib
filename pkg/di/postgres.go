package gtdi

import (
	"database/sql"
	"errors"
	"fmt"
)

var ErrPsqlNotInitialized = errors.New("psql instance is not initialized")
var ErrPsqlCannotConnect = errors.New("cannot connect to postgres database")
var ErrPsqlDbPing = errors.New("postgres database ping error")

type Postgres *sql.DB

type (
	PostgresConfig struct {
		User     string
		Password string
		Port     uint
		DbName   string
	}
)

func ProvidePostgres(cfg PostgresConfig) error {
	return providePlugin[Postgres](func() (Postgres, error) {
		dbSrc := fmt.Sprintf("postgresql://%s:%s@localhost:%d/%s?sslmode=disable", cfg.User, cfg.Password, cfg.Port, cfg.DbName)

		conn, err := sql.Open("postgres", dbSrc)
		if err != nil {
			return nil, fmt.Errorf("%v: %v", ErrPsqlCannotConnect, err)
		}

		err = conn.Ping()
		if err != nil {
			return nil, fmt.Errorf("%v: %v", ErrPsqlDbPing, err)
		}

		ProvideObject[Postgres](conn)

		return conn, nil
	})
}
