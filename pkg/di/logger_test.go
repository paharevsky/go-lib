package gtdi

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestProvideCommonLogger(t *testing.T) {
	createInjector[*testDiConfig](t, nil, nil)

	err := ProvideLogger(LoggerConfig{})
	require.NoError(t, err)

	logger := MustInvoke[Logger]()
	require.NotEmpty(t, logger)
}

func TestProvideJSONLogger(t *testing.T) {
	createInjector[*testDiConfig](t, nil, nil)

	err := ProvideLogger(LoggerConfig{
		Format: LoggerFormatJSON,
	})
	require.NoError(t, err)

	logger := MustInvoke[Logger]()
	require.NotEmpty(t, logger)
}
