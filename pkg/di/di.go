// Package gtdi
// @Description: Управление внедрением зависимостей (DI/Dependency injection).
package gtdi

import (
	"errors"
	"github.com/samber/do"
)

var ErrInjectorNotCreated = errors.New("injector not created")

var injector *do.Injector

// Init Init[T comparable]
//
//	@Description: Инициализация di контейнера.
//	@param cfg Конфигурация приложения.
func Init[T comparable](cfg T, fnProvider func(cfg T)) {
	injector = do.New()
	if fnProvider != nil {
		fnProvider(cfg)
	}
}

// IsInjectorCreated
//
//	@Description: Инъектор инициализирован.
//	@return bool true-да, false-нет.
func IsInjectorCreated() bool {
	if injector == nil {
		return false
	}
	return true
}

// IsInjectorNotCreated
//
//	@Description: Инъектор не инициализирован.
//	@return bool true-да, false-нет.
func IsInjectorNotCreated() bool {
	return !IsInjectorCreated()
}

// GetInjector
//
//	@Description: Получить объект инъектора.
//	@return *do.Injector Объект инъектора.
func GetInjector() *do.Injector {
	return injector
}

// Invoke Invoke[T any]
//
//	@Description: Получить экземпляр объекта.
//	@return T Экземпляр объекта.
//	@return error Ошибка, если != nil, то объект не зарегистрирован в di контейнере.
func Invoke[T any]() (T, error) {
	return do.Invoke[T](injector)
}

// MustInvoke MustInvoke[T any]
//
//	@Description: Получить экземпляр объекта из di контейнера, который точно инициализирован.
//	@return T Экземпляр объекта.
func MustInvoke[T any]() T {
	return do.MustInvoke[T](injector)
}

// MustInvokeNamed MustInvokeNamed[T any]
//
//	@Description: Получить экземпляр объекта по его имени из di контейнера.
//	@param name Название объекта.
//	@return T Экземпляр объекта.
func MustInvokeNamed[T any](name string) T {
	return do.MustInvokeNamed[T](injector, name)
}

// ProvideAdvConstructor ProvideAdvConstructor[T any, K comparable]
//
//	@Description: Зарегистрировать объект через конструктор, который принимает конфиг.
//	@param constructor Функция-конструктор.
//	@param constructorCfg Конфигурация конструктора.
func ProvideAdvConstructor[T any, K comparable](constructor func(K) (T, error), constructorCfg K) {
	Provide[T](func(i *do.Injector) (T, error) {
		return constructor(constructorCfg)
	})
}

// ProvideConstructor ProvideConstructor[T any]
//
//	@Description: Зарегистрировать объект через конструктор.
//	@param constructor Функция-конструктор.
func ProvideConstructor[T any](constructor func() (T, error)) {
	Provide[T](func(i *do.Injector) (T, error) {
		return constructor()
	})
}

// ProvideObject ProvideObject[T any]
//
//	@Description: Зарегистрировать объект.
//	@param obj Объект для регистрации.
func ProvideObject[T any](obj T) {
	Provide[T](func(i *do.Injector) (T, error) {
		return obj, nil
	})
}

// Provide Provide[T any]
//
//	@Description: Зарегистрировать объект, используя callback функцию.
//	@param fn Callback функция-конструктор.
func Provide[T any](fn func(i *do.Injector) (T, error)) {
	do.Provide(injector, fn)
}

// ProvideNamedValue
//
//	@Description: Зарегистрировать объект, используя ключевое слово.
//	@param name Ключевое слово.
//	@param obj Объект.
func ProvideNamedValue(name string, obj any) {
	do.ProvideNamedValue(injector, name, obj)
}

// Override Override[T any]
//
//	@Description: Перезаписать объект, используя callback функцию.
//	@param fn Callback функция-конструктор.
func Override[T any](fn func(i *do.Injector) (T, error)) {
	do.Override[T](injector, fn)
}

// OverrideAdvConstructor OverrideAdvConstructor[T any, K comparable]
//
//	@Description: Перезаписать объект через конструктор, который принимает конфиг.
//	@param constructor Функция-конструктор.
//	@param constructorCfg Конфиг конструктора.
func OverrideAdvConstructor[T any, K comparable](constructor func(K) (T, error), constructorCfg K) {
	Override[T](func(i *do.Injector) (T, error) {
		return constructor(constructorCfg)
	})
}

// OverrideConstructor OverrideConstructor[T any]
//
//	@Description: Перезаписать объект через конструктор.
//	@param constructor Функция конструктор.
func OverrideConstructor[T any](constructor func() (T, error)) {
	Override[T](func(i *do.Injector) (T, error) {
		return constructor()
	})
}

// OverrideObject OverrideObject[T any]
//
//	@Description: Перезаписать объект.
//	@param obj Объект для перезаписи.
func OverrideObject[T any](obj T) {
	Override[T](func(i *do.Injector) (T, error) {
		return obj, nil
	})
}

// providePlugin[T any]
//
//	@Description: Зарегистрировать плагин.
//	@param fn Функция инициализации плагина.
//	@return error Ошибка.
func providePlugin[T any](fn func() (T, error)) error {
	if IsInjectorNotCreated() {
		return ErrInjectorNotCreated
	}

	obj, err := fn()
	if err != nil {
		return err
	}

	ProvideObject[T](obj)

	return nil
}
