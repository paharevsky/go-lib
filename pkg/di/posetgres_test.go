package gtdi

import (
	"github.com/stretchr/testify/require"
	"testing"
)

var cfg = PostgresConfig{
	User:     "test",
	Password: "",
	Port:     5432,
	DbName:   "test",
}

func TestDi_ProvidePostgres(t *testing.T) {
	err := ProvidePostgres(cfg)
	require.Error(t, err, "Test ProvidePostgres")
}
