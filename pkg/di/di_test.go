package gtdi

import (
	"github.com/samber/do"
	"github.com/stretchr/testify/require"
	"testing"
)

type testDiConfig struct{}

func createInjector[K comparable](t *testing.T, cfg K, fnProvider func(cfg K)) *do.Injector {
	defer func() {
		if r := recover(); r != nil {
			t.Error("unexpected panic")
		}
	}()
	Init(cfg, fnProvider)
	in := GetInjector()

	require.NotEmpty(t, in)

	return in
}

func TestIsInjectorNotCreated(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Error("unexpected panic")
		}
	}()
	require.True(t, IsInjectorNotCreated())
}

func TestIsInjectorCreated(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Error("unexpected panic")
		}
	}()
	createInjector[*testDiConfig](t, nil, nil)
	require.True(t, IsInjectorCreated())
}

func TestDi_Init(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Error("unexpected panic")
		}
	}()
	Init[struct{}](struct{}{}, func(cfg struct{}) {})
}

func TestDi_ProvideAdvConstructor(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Error("unexpected panic")
		}
	}()
	constr := func(a string) (struct{}, error) {
		return struct{}{}, nil
	}
	ProvideAdvConstructor[struct{}, string](constr, "test")
}

func TestDi_ProvideConstructor(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Error("unexpected panic")
		}
	}()
	constr1 := func() (string, error) {
		return "", nil
	}
	ProvideConstructor[string](constr1)
}

func TestDi_Provide(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Error("unexpected panic")
		}
	}()
	Provide[int](func(i *do.Injector) (int, error) {
		return 0, nil
	})
}

func TestDi_ProvideNamedValue(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Error("unexpected panic")
		}
	}()
	ProvideNamedValue("some", struct {
		name string
	}{})
}

func TestDi_Override(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Error("unexpected panic")
		}
	}()
	Override[string](func(i *do.Injector) (string, error) {
		return "", nil
	})
}

func TestDi_OverrideAdvConstructor(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Error("unexpected panic")
		}
	}()
	OverrideAdvConstructor[struct{}, string](func(a string) (struct{}, error) {
		return struct{}{}, nil
	}, "")
}

func TestDi_OverrideConstructor(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Error("unexpected panic")
		}
	}()
	OverrideConstructor[string](func() (string, error) {
		return "", nil
	})
}

func TestDi_OverrideObject(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Error("unexpected panic")
		}
	}()
	OverrideObject[string]("")
}

func TestDi_Invoke(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Error("unexpected panic")
		}
	}()
	_, err := Invoke[string]()
	require.Nilf(t, err, "unexpected error: %s", err)
}

func TestDi_MustInvokeNamed(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Error("unexpected panic")
		}
	}()
	_ = MustInvokeNamed[string]("string")
}
