// Package gtfmt
// @Description: Инструменты для работы с форматированным выводом логов.
package gtfmt

import (
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"strings"
)

var ErrRequiredId = errors.New("id is required")

type NewCfg struct {
	Id          string
	Logger      *log.Logger
	EnableAuto  bool
	ForkDivider string
}

type Fmt struct {
	id          string
	logger      *log.Logger
	enableAuto  bool
	forkDivider string
}

// New
//
//	@Description: Конструктор форматтера.
//	@param cfg Конфигурация форматтера.
//	@return *Fmt Объект форматтера.
//	@return error Ошибка.
func New(cfg NewCfg) (*Fmt, error) {
	if strings.TrimSpace(cfg.Id) == "" {
		return nil, ErrRequiredId
	}

	if cfg.ForkDivider == "" {
		cfg.ForkDivider = "::"
	}

	return &Fmt{
		id:          cfg.Id,
		logger:      cfg.Logger,
		enableAuto:  cfg.EnableAuto,
		forkDivider: cfg.ForkDivider,
	}, nil
}

// Fork
//
//	@Description: Создать новый дочерний форматтер.
//	@receiver f Форматтер.
//	@param id Id форматтера.
//	@return *Fmt Дочерний объект форматтера.
//	@return error Ошибка.
func (f *Fmt) Fork(id string) (*Fmt, error) {
	if strings.TrimSpace(id) == "" {
		return nil, ErrRequiredId
	}

	forkId := fmt.Sprintf("%v%v%v", f.id, f.forkDivider, id)

	return New(NewCfg{
		Id:         forkId,
		Logger:     f.logger,
		EnableAuto: f.enableAuto,
	})
}

// Print
//
//	@Description: Вывести лог с автоматическим определением типа (ошибка или нет).
//	@receiver f Форматтер.
//	@param msg Информация, которую надо вывести.
func (f *Fmt) Print(msg ...any) {
	isError := false
	id := "[" + f.id + "]"
	str := ""

	// Эти две переменные создаются на верхнем уровне,
	// чтобы не создавать подобные переменные на каждой итерации.
	errOk := false
	var tempErr error

	for _, m := range msg {
		if m == "" {
			continue
		}

		if m == nil {
			m = "nil"
		} else {
			tempErr, errOk = m.(error)
			if errOk {
				isError = true
				if tempErr == nil || strings.TrimSpace(tempErr.Error()) == "" {
					m = "unknown error"
				}
				str = fmt.Sprintf("%v error: '%v'", str, m)
				continue
			}
		}

		str = fmt.Sprintf("%v %v", str, m)
	}

	if isError {
		if f.logger != nil {
			str = fmt.Sprintf("%v%v", id, str)
			f.logger.Error(str)
		} else {
			str = fmt.Sprintf("%v <ERROR>%v", id, str)
			fmt.Println(str)
		}
	} else {
		str = fmt.Sprintf("%v%v", id, str)
		if f.logger != nil {
			f.logger.Print(str)
		} else {
			fmt.Println(str)
		}
	}
}

// AutoPrint
//
//	@Description: Вывести лог с автоматическим определением типа (ошибка или нет). Лог выводится, если enableAuto=true.
//	@receiver f Форматтер.
//	@param msg Информация, которую надо вывести.
func (f *Fmt) AutoPrint(msg ...any) {
	if !f.enableAuto {
		return
	}

	f.Print(msg...)
}

// IfError
//
//	@Description: Вывести лог, только если в нем содержится ошибка.
//	@receiver f Форматтер.
//	@param msg Информация, которую надо вывести.
func (f *Fmt) IfError(msg ...any) {
	for _, m := range msg {
		_, errOk := m.(error)
		if errOk {
			f.Print(msg...)
			return
		}
	}
}

// AutoIfError
//
//	@Description: Вывести лог с автоматическим определением типа (ошибка или нет). Лог выводится, если enableAuto=true.
//	@receiver f Форматтер.
//	@param msg Информация, которую надо вывести.
func (f *Fmt) AutoIfError(msg ...any) {
	if !f.enableAuto {
		return
	}

	f.IfError(msg...)
}

// SetId
//
//	@Description: Установить новый id. Может быть полезно при использовании DI.
//	@receiver f Форматтер
//	@param id Новый id.
func (f *Fmt) SetId(id string) {
	f.id = id
}
