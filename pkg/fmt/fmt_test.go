package gtfmt

import (
	"errors"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	"testing"
)

var cfg = NewCfg{
	Id:          "",
	Logger:      nil,
	EnableAuto:  false,
	ForkDivider: "",
}

var newFmt *Fmt

func TestFmt_New(t *testing.T) {
	_, err := New(cfg)
	if err == nil {
		t.Error("expected error")
	}
	cfg.Id = "1"
	newFmt, err = New(cfg)
	require.Nilf(t, err, "unexpected error: %s", err)
	require.NotNil(t, newFmt, "unexpected nil New fmt")
}

func TestFmt_Fork(t *testing.T) {
	_, err := newFmt.Fork("")
	require.Errorf(t, err, "In test %s expected error", "Empty id")

	forkFmt, err := newFmt.Fork("2")
	require.Nilf(t, err, "In test %s unexpected error: %s", "Ok id", err)
	require.NotNilf(t, forkFmt, "In test %s unexpected nil fork fmt", "Ok id")
}

func TestFmt_Print(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Error("panicked while printing")
		}
	}()
	err := errors.New("test error")
	newFmt.Print("test")
	newFmt.Print("")
	newFmt.Print(nil)
	newFmt.Print(err)
	emptyErr := errors.New("")
	newFmt.Print(emptyErr)
	cfg.Logger = log.New()
	logFmt, err := New(cfg)
	require.Nilf(t, err, "In test %s unexpected error: %s", "New log", err)
	logFmt.Print("test with logger")
	err = errors.New("test error logger")
	logFmt.Print(err)
}

func TestFmt_AutoPrint(t *testing.T) {
	newFmt.AutoPrint("should not print")
	cfg.EnableAuto = true
	withAutoFmt, err := New(cfg)
	require.Nilf(t, err, "In test %s unexpected error: %s", "New auto log", err)
	withAutoFmt.AutoPrint("test auto print")
}

func TestFmt_IsError(t *testing.T) {
	newFmt.IfError("should not print")
	err := errors.New("error test is error")
	newFmt.IfError(err)
}

func TestFmt_AutoIsError(t *testing.T) {
	err := errors.New("error test auto is error")
	newFmt.AutoIfError(err)
	cfg.EnableAuto = true
	withAutoFmt, err := New(cfg)
	require.Nilf(t, err, "In test %s unexpected error: %s", "New auto error log", err)
	withAutoFmt.AutoIfError(err)
}

func TestFmt_SetId(t *testing.T) {
	testId := "2"
	newFmt.SetId(testId)
	require.Equalf(t, testId, newFmt.id, "In test %s set wrong id ", " SetId")
}
