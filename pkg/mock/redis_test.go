package gtmock

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestMock_RedisClient(t *testing.T) {
	cmd, client := RedisClient()
	require.NotNilf(t, cmd, "In test %s invalid redis cmdable instance", "RedisClient")
	require.NotNilf(t, client, "In test %s invalid redis client instance", "RedisClient")
}

func TestMock_OverrideDIRedisClient(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Error("panicked while overriding di redis client")
		}
	}()
	OverrideDIRedisClient()
}
