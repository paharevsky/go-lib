package gtmock

import (
	"github.com/go-redis/redismock/v9"
	"github.com/redis/go-redis/v9"
	gtdi "gitlab.com/paharevsky/go-lib/pkg/di"
	"sync"
)

var onceOverrideDIRedisClient sync.Once

// RedisClient
//
//	@Description: Создать тестовый Redis.
//	@return redis.Cmdable База данных.
//	@return redismock.ClientMock Клиент.
func RedisClient() (redis.Cmdable, redismock.ClientMock) {
	return redismock.NewClientMock()
}

// OverrideDIRedisClient
//
//	@Description: Перезаписать БД Redis в DI контейнере.
func OverrideDIRedisClient() {
	onceOverrideDIRedisClient.Do(func() {
		db, _ := RedisClient()
		gtdi.OverrideObject[redis.Cmdable](db)
	})
}
