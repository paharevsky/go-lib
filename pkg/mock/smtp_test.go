package gtmock

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestMock_SmtpServer(t *testing.T) {
	err := SmtpServer(SmtpServerCfg{})
	require.Nilf(t, err, "In test %s unexpected error: %s", "SmtpServer start", err)
}
