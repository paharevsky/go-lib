package gtmock

import (
	smtpmock "github.com/mocktools/go-smtp-mock/v2"
	"sync"
)

var smtpServer *smtpmock.Server
var onceSmtpServer sync.Once

type SmtpServerCfg smtpmock.ConfigurationAttr

// SmtpServer
//
//	@Description: Создать новый smtp mock сервер.
//	@param cfg Конфигурация smtp mock сервера.
//	@return *smtpmock.Server Smtp mock сервер.
func SmtpServer(cfg SmtpServerCfg) error {
	var err error

	onceSmtpServer.Do(func() {
		smtpServer = smtpmock.New(smtpmock.ConfigurationAttr(cfg))
		err = smtpServer.Start()
	})

	return err
}
