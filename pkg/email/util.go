// Package gtemail
// @Description: Инструменты для отправки писем по email.
package gtemail

import (
	"bytes"
	"html/template"
)

// ParseTemplate
//
//	@Description: Парсинг html-шаблона письма.
//	@param path Путь до файла, включая сам файл.
//	@param data Данные, которые необходимо подставить при парсинге.
//	@return []byte HTML-шаблон.
//	@return error Ошибка.
func ParseTemplate(path string, data interface{}) ([]byte, error) {
	t, err := template.ParseFiles(path)
	if err != nil {
		return nil, err
	}
	buf := new(bytes.Buffer)
	if err := t.Execute(buf, data); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
