package gtemail

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestEmail_ParseTemplate(t *testing.T) {
	_, err := ParseTemplate("", nil)
	require.Error(t, err, "expected error invalid path")

	by, err := ParseTemplate("template/welcome.html", nil)
	require.Nilf(t, err, "unexpected error: %s", err)
	require.NotNil(t, by, "unexpected nil bytes")
}
