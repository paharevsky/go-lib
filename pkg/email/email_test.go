package gtemail

import (
	"github.com/stretchr/testify/require"
	"testing"
)

var sender *Sender

func TestEmail_New(t *testing.T) {
	sender = New(nil, SenderConfig{
		SMTPHost:      "",
		SMTPPort:      0,
		Email:         "",
		EmailUsername: "",
		EmailPassword: "",
	})
	require.NotNil(t, sender, "unexpected nil sender")
}

func TestEmail_NewMsg(t *testing.T) {
	msg := sender.newMsg([]string{}, "test", []byte("test"))
	require.NotNil(t, msg, "unexpected nil message")

}

func TestEmail_Send(t *testing.T) {
	err := sender.Send("", "test", []byte("test"))
	require.Error(t, err, "expected error")
}

func TestEmail_MassSend(t *testing.T) {
	err := sender.MassSend([]string{}, "test", []byte("test"))
	require.Error(t, err, "expected error")
}
