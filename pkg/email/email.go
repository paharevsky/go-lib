// Package gtemail
// @Description: Инструменты для отправки писем по email.
package gtemail

import (
	log "github.com/sirupsen/logrus"
	"gopkg.in/gomail.v2"
)

// SenderConfig
// @Description: Конфигурация SMTP.
type SenderConfig struct {
	SMTPHost      string `yaml:"host"`
	SMTPPort      int    `yaml:"port"`
	Email         string `yaml:"email"`
	EmailUsername string `yaml:"username"`
	EmailPassword string `yaml:"password"`
}

// Sender
// @Description: Структура для работы с email.
type Sender struct {
	logger     *log.Logger
	smtpClient *gomail.Dialer
	cfg        SenderConfig
}

// New
//
//	@Description: Создание нового объекта для отправления email.
//	@param logger Логгер.
//	@param cfg Конфигурация SMTP.
//	@return *Sender Структура для работы с email.
func New(logger *log.Logger, cfg SenderConfig) *Sender {
	smtpClient := gomail.NewDialer(cfg.SMTPHost, cfg.SMTPPort, cfg.EmailUsername, cfg.EmailPassword)

	return &Sender{
		logger:     logger,
		smtpClient: smtpClient,
		cfg:        cfg,
	}
}

// newMsg
//
//	@Description: Получить новый объект сообщения, подготовленного для отправки.
//	@receiver s
//	@param toEmails Получатели письма.
//	@param subject Тема письма.
//	@param body Содержимое письма.
//	@return *gomail.Message Новый объект сообщения, подготовленного для отправки.
func (s *Sender) newMsg(toEmails []string, subject string, body []byte) *gomail.Message {
	m := gomail.NewMessage()
	m.SetHeader("From", s.cfg.Email)
	m.SetHeader("To", toEmails...)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", string(body))

	return m
}

// Send
//
//	@Description: Отправка email.
//	@receiver es
//	@param toEmail Получатель письма.
//	@param subject Тема письма.
//	@param body Содержимое письма.
//	@return error Ошибка.
func (s *Sender) Send(toEmail string, subject string, body []byte) error {
	return s.MassSend([]string{toEmail}, subject, body)
}

// MassSend
//
//	@Description: Отправка email различным получателям.
//	@receiver es
//	@param toEmails Получатели письма.
//	@param subject Тема письма.
//	@param body Содержимое письма.
//	@return error Ошибка.
func (s *Sender) MassSend(toEmails []string, subject string, body []byte) error {
	m := s.newMsg(toEmails, subject, body)

	err := s.smtpClient.DialAndSend(m)
	if err != nil {
		return err
	}

	return nil
}
